use crate::exercises::{first_digit, last_digit};
use phf::phf_map;

static DIGITS: phf::Map<&'static str, char> = phf_map! {
    "zero" => '0',
    "one" => '1',
    "two" => '2',
    "three" => '3',
    "four" => '4',
    "five" => '5',
    "six" => '6',
    "seven" => '7',
    "eight" => '8',
    "nine" => '9',
};

pub fn day1_2(input: &str) -> String {
    let lines = input.split('\n');
    lines
        .map(find_digits)
        .map(|line| 10 * first_digit(&line) + last_digit(&line))
        .sum::<u32>()
        .to_string()
}

fn find_digits(text: &str) -> String {
    let mut all_digits = String::new();
    let mut search_text = String::from(text);
    while !search_text.is_empty() {
        let first_char = search_text.chars().next().unwrap();
        if first_char.is_ascii_digit() {
            all_digits.push(first_char);
            search_text.remove(0);
            continue;
        }
        if let Some(digit) = find_text_digit_at_start(&search_text) {
            all_digits.push(digit);
        };
        search_text.remove(0);
    }
    all_digits
}

fn find_text_digit_at_start(text: &str) -> Option<char> {
    for (text_digit, digit) in DIGITS.entries() {
        if text.starts_with(text_digit) {
            return Some(*digit);
        }
    }
    None
}

#[allow(dead_code)]
fn replace_digits(text: &str) -> String {
    // This doesn't work, since multiple digits can overlap
    // e.g. twone → 21
    let mut output_text = String::from(text);
    for (from, to) in DIGITS.entries() {
        output_text = output_text.replace(from, &to.to_string());
    }
    output_text.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn website_example() {
        let input = "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";
        assert_eq!("281", day1_2(input));
    }

    #[test]
    fn replace_one_with_1() {
        assert_eq!("11", replace_digits("one1"))
    }

    #[test]
    fn replace_more_digits() {
        assert_eq!("012359", replace_digits("zeroonetwothree5nine"))
    }

    #[test]
    fn find_all_ones() {
        assert_eq!("111", find_digits("one1one"));
    }
}
