pub use crate::exercises::day1_1::*;
pub use crate::exercises::day1_2::*;
pub use crate::exercises::day2::*;
pub use crate::exercises::day3::*;
pub use crate::exercises::day4::*;

pub mod day1_1;
pub mod day1_2;
pub mod day2;
pub mod day3;
pub mod day4;
