use std::{cmp::min, collections::HashMap};

#[allow(dead_code)]
struct Card {
    id: u32,
    winning: Vec<u32>,
    draw: Vec<u32>,
}

impl Card {
    fn hits(&self) -> Vec<u32> {
        self.draw
            .clone()
            .into_iter()
            .filter(|n| self.winning.contains(n))
            .collect()
    }
}

pub fn day4_1(input: &str) -> String {
    let lines = input.trim().split_terminator('\n');
    lines
        .map(parse_card)
        .map(|card| card.hits())
        .filter(|hits| !hits.is_empty())
        .map(|hits| 2_u32.pow(u32::try_from(hits.len()).unwrap() - 1))
        .sum::<u32>()
        .to_string()
}

pub fn day4_2(input: &str) -> String {
    let lines = input.trim().split_terminator('\n');
    let mut card_counts: HashMap<u32, u32> = HashMap::new();
    let cards: Vec<Card> = lines.map(parse_card).collect();
    for card in &cards {
        let end = min(
            card.id + u32::try_from(card.hits().len()).unwrap() + 1,
            u32::try_from(cards.len()).unwrap(),
        );
        let card_count: u32 = *card_counts
            .entry(card.id)
            .and_modify(|counter| *counter += 1)
            .or_insert(1);
        for i in card.id + 1..end {
            card_counts
                .entry(i)
                .and_modify(|counter| *counter += card_count)
                .or_insert(card_count);
        }
    }
    card_counts.values().sum::<u32>().to_string()
}

fn parse_card(line: &str) -> Card {
    let mut card_parts = line.split(": ");
    let header = card_parts.next().unwrap();
    let id = header
        .strip_prefix("Card")
        .expect("Invalid card header!")
        .trim()
        .parse::<u32>()
        .expect("Invalid card ID")
        - 1;
    let mut number_parts = card_parts.next().unwrap().split(" | ");
    let winning = number_parts
        .next()
        .unwrap()
        .split_whitespace()
        .map(|val| {
            val.parse::<u32>()
                .expect("Card contains an invalid number!")
        })
        .collect();
    let draw = number_parts
        .next()
        .unwrap()
        .split_whitespace()
        .map(|val| {
            val.parse::<u32>()
                .expect("Card contains an invalid number!")
        })
        .collect();
    Card { id, winning, draw }
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = "
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

    #[test]
    fn website_example() {
        assert_eq!("13", day4_1(INPUT));
    }

    #[test]
    fn website_example_2() {
        assert_eq!("30", day4_2(INPUT));
    }
}
