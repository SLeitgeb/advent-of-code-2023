#[derive(Debug, Clone, Copy)]
struct Turn {
    red: usize,
    green: usize,
    blue: usize,
}

#[derive(Debug)]
struct Game {
    id: usize,
    turns: Vec<Turn>,
}

pub fn day2_1(input: &str) -> String {
    let lines = input.split_terminator('\n');
    let max_turn = Turn {
        red: 12,
        green: 13,
        blue: 14,
    };
    lines
        .map(parse_game)
        .filter(|game| no_turns_over_x(game, max_turn))
        .map(|game| game.id)
        .sum::<usize>()
        .to_string()
}

pub fn day2_2(input: &str) -> String {
    let lines = input.split_terminator('\n');
    lines
        .map(parse_game)
        .map(least_cubes_for_game)
        .map(|turn| turn.red * turn.green * turn.blue)
        .sum::<usize>()
        .to_string()
}

fn least_cubes_for_game(game: Game) -> Turn {
    let mut min_cubes = Turn {
        red: 0,
        green: 0,
        blue: 0,
    };
    for turn in &game.turns {
        if turn.red > min_cubes.red {
            min_cubes.red = turn.red;
        }
        if turn.green > min_cubes.green {
            min_cubes.green = turn.green;
        }
        if turn.blue > min_cubes.blue {
            min_cubes.blue = turn.blue;
        }
    }
    min_cubes
}

fn no_turns_over_x(game: &Game, max_turn: Turn) -> bool {
    for turn in &game.turns {
        if turn.red > max_turn.red || turn.green > max_turn.green || turn.blue > max_turn.blue {
            return false;
        }
    }
    true
}

fn parse_game(line: &str) -> Game {
    let mut game_parts = line.split(": ");
    let game_id: usize = if let Some(header_part) = game_parts.next() {
        header_part.split(' ').collect::<Vec<&str>>()[1]
            .parse::<usize>()
            .unwrap()
    } else {
        panic!("Invalid game ID!")
    };
    let game_turns = if let Some(turn_part) = game_parts.next() {
        turn_part.split("; ").map(parse_turn).collect()
    } else {
        vec![]
    };
    Game {
        id: game_id,
        turns: game_turns,
    }
}

fn parse_turn(turn: &str) -> Turn {
    let mut reds = 0;
    let mut greens = 0;
    let mut blues = 0;
    let mut cube_groups = turn.split(", ");
    let mut cube_group = cube_groups.next();
    while cube_group.is_some() {
        let cubes = cube_group.unwrap().split(' ').collect::<Vec<&str>>();
        let cube_count = cubes[0].parse::<usize>().unwrap();
        match cubes[1] {
            "red" => reds += cube_count,
            "green" => greens += cube_count,
            "blue" => blues += cube_count,
            _ => panic!("Invalid cube color!"),
        }
        cube_group = cube_groups.next();
    }
    Turn {
        red: reds,
        green: greens,
        blue: blues,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn website_example() {
        let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
        assert_eq!("8", day2_1(input));
    }

    #[test]
    fn website_example_part_2() {
        let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
        assert_eq!("2286", day2_2(input));
    }
}
