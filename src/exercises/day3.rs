use std::{cmp::min, str::SplitTerminator};

#[derive(Clone, Copy, Debug, PartialEq)]
enum ItemDetails {
    PartNumber(u32),
    NodeSymbol(char),
}

#[allow(dead_code)]
#[derive(Clone, Copy, Debug)]
struct SchematicItem {
    x: usize,
    y: usize,
    size: usize,
    neighbourhood: (usize, usize, usize, usize),
    details: ItemDetails,
}

#[allow(dead_code)]
impl SchematicItem {
    fn new_part(part_no: u32, x_last: usize, y: usize) -> Self {
        let size = (part_no.ilog10() + 1).try_into().unwrap();
        let x = x_last - size;
        let neighbourhood = Self::neighbourhood(x, y, size);
        Self {
            x,
            y,
            size,
            neighbourhood,
            details: ItemDetails::PartNumber(part_no),
        }
    }

    fn new_symbol(symbol: char, x: usize, y: usize) -> Self {
        let neighbourhood = Self::neighbourhood(x, y, 1);
        Self {
            x,
            y,
            size: 1,
            neighbourhood,
            details: ItemDetails::NodeSymbol(symbol),
        }
    }

    fn get_part_number(&self) -> u32 {
        match self.details {
            ItemDetails::PartNumber(no) => no,
            _ => unimplemented!("All parts in the part map must have a PartNumber!"),
        }
    }

    fn neighbourhood(x: usize, y: usize, size: usize) -> (usize, usize, usize, usize) {
        let x_min = x.saturating_sub(1);
        let x_max = x + size;
        let y_min = y.saturating_sub(1);
        let y_max = y + 1;
        (x_min, x_max, y_min, y_max)
    }

    fn item_between_rows(&self, y_min: usize, y_max: usize) -> bool {
        (y_min..=y_max + 1).contains(&self.y)
    }

    fn item_between_cols(&self, x_min: usize, x_max: usize) -> bool {
        (x_min..=x_max).contains(&self.x) || (x_min..=x_max).contains(&(self.x + self.size - 1))
    }

    fn item_near_another_item(&self, other: Self) -> bool {
        let (x_min, x_max, y_min, y_max) = self.neighbourhood;
        other.item_between_rows(y_min, y_max) && other.item_between_cols(x_min, x_max)
    }
}

fn items_near_an_item(
    item: &SchematicItem,
    item_map: &Vec<Vec<SchematicItem>>,
) -> Vec<SchematicItem> {
    let (x_min, x_max, y_min, y_max) = item.neighbourhood;
    let y_max = min(y_max + 1, item_map.len());
    item_map[y_min..y_max]
        .iter()
        .flatten()
        .filter(|other| other.item_between_cols(x_min, x_max))
        .copied()
        .collect()
}

pub fn day3_1(input: &str) -> String {
    let lines = input.trim().split_terminator('\n');
    let (part_map, symbol_map) = parse_schematic(lines);
    part_map
        .iter()
        .flatten()
        .filter(|part| !items_near_an_item(part, &symbol_map).is_empty())
        .map(SchematicItem::get_part_number)
        .sum::<u32>()
        .to_string()
}

pub fn day3_2(input: &str) -> String {
    let lines = input.trim().split_terminator('\n');
    let (part_map, symbol_map) = parse_schematic(lines);
    symbol_map
        .iter()
        .flatten()
        .filter(|sym| sym.details == ItemDetails::NodeSymbol('*'))
        .map(|sym| items_near_an_item(sym, &part_map))
        .filter(|parts| parts.len() > 1)
        .map(|parts| {
            parts
                .iter()
                .map(SchematicItem::get_part_number)
                .product::<u32>()
        })
        .sum::<u32>()
        .to_string()
}

fn parse_schematic(
    lines: SplitTerminator<'_, char>,
) -> (Vec<Vec<SchematicItem>>, Vec<Vec<SchematicItem>>) {
    let mut part_map: Vec<Vec<SchematicItem>> = vec![];
    let mut line_index: usize = 0;
    // using bit masks would be much more efficient
    let symbol_map = lines
        .map(|line| {
            let mut symbols = vec![];
            let mut parts: Vec<SchematicItem> = vec![];
            let mut maybe_part: Option<u32> = None;
            let mut chars = line.chars();
            let chars_len = chars.clone().count();
            for i in 0..chars_len {
                match chars.next() {
                    Some(d) if d.is_ascii_digit() => {
                        let new_digit = d.to_digit(10).unwrap_or_default();
                        maybe_part = Some(
                            new_digit
                                + match maybe_part {
                                    Some(part_no) => 10 * part_no,
                                    None => 0,
                                },
                        );
                    }
                    Some(c) => {
                        if c != '.' {
                            symbols.push(SchematicItem::new_symbol(c, i, line_index));
                        }
                        if let Some(part_no) = maybe_part {
                            parts.push(SchematicItem::new_part(part_no, i, line_index));
                            maybe_part = None;
                        }
                    }
                    None => panic!("More characters were advertised!"),
                }
            }
            if let Some(part_no) = maybe_part {
                parts.push(SchematicItem::new_part(part_no, chars_len, line_index));
            }
            part_map.push(parts);
            line_index += 1;
            symbols
        })
        .collect();
    (part_map, symbol_map)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn website_example() {
        let input = "
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";
        assert_eq!("4361", day3_1(input));
    }

    #[test]
    fn website_example_part_2() {
        let input = "
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";
        assert_eq!("467835", day3_2(input));
    }
}
