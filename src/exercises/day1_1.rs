pub fn day1_1(input: &str) -> String {
    let lines = input.split('\n');
    lines
        .map(|line| 10 * first_digit(line) + last_digit(line))
        .sum::<u32>()
        .to_string()
}

pub fn first_digit(line: &str) -> u32 {
    line.chars()
        .find(char::is_ascii_digit)
        .unwrap_or('0')
        .to_digit(10)
        .unwrap_or_default()
}

pub fn last_digit(line: &str) -> u32 {
    // An illustrative second option.
    // Works only with ASCII digits (0-9)
    // This would also convert non-digit characters as digits, causing all sorts of problems.
    line.chars().rev().find(char::is_ascii_digit).unwrap_or('0') as u32 - '0' as u32
}

#[cfg(test)]
mod tests {
    use crate::exercises::day1_1;
    #[test]
    fn website_example() {
        let input = "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";
        assert_eq!("142", day1_1(input));
    }
}
