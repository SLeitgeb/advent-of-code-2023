use advent_of_code_2023::exercises::day1_1;
use advent_of_code_2023::exercises::day1_2;
use advent_of_code_2023::exercises::day2_1;
use advent_of_code_2023::exercises::day2_2;
use advent_of_code_2023::exercises::day3_1;
use advent_of_code_2023::exercises::day3_2;
use advent_of_code_2023::exercises::day4_1;
use advent_of_code_2023::exercises::day4_2;
use std::env;
use std::fs;
use std::io;

const DEFAULT_DAY: &str = "day4";
const DEFAULT_EXERCISE: &str = "day4_2";

fn read_from_stdin() -> Result<String, io::Error> {
    io::read_to_string(io::stdin())
}

fn load_file(file: &str) -> Result<String, io::Error> {
    fs::read_to_string(file)
}

fn run_with_args(mut args: env::Args) -> String {
    args.next();
    let arg = args.next();
    let exercise = match arg.as_deref() {
        Some(value) => value,
        None => DEFAULT_EXERCISE,
    };
    let exercise_input = match args.next().as_deref() {
        Some("-") => read_from_stdin(),
        Some(filename) => load_file(filename),
        None => load_file(format!("input/{DEFAULT_DAY}").as_str()),
    }
    .unwrap();

    match exercise {
        "day1_1" => day1_1(&exercise_input),
        "day1_2" => day1_2(&exercise_input),
        "day2_1" => day2_1(&exercise_input),
        "day2_2" => day2_2(&exercise_input),
        "day3_1" => day3_1(&exercise_input),
        "day3_2" => day3_2(&exercise_input),
        "day4_1" => day4_1(&exercise_input),
        "day4_2" => day4_2(&exercise_input),
        _ => unimplemented!("Exercise not implemented yet!"),
    }
}

fn main() {
    let args = env::args();
    let output = run_with_args(args);
    println!("{}", output);
}
